package ro.deloitte.digital.jhipster.conferences.web.rest;

import ro.deloitte.digital.jhipster.conferences.ConferencesApp;

import ro.deloitte.digital.jhipster.conferences.config.SecurityBeanOverrideConfiguration;

import ro.deloitte.digital.jhipster.conferences.domain.Conference;
import ro.deloitte.digital.jhipster.conferences.repository.ConferenceRepository;
import ro.deloitte.digital.jhipster.conferences.service.ConferenceService;
import ro.deloitte.digital.jhipster.conferences.service.dto.ConferenceDTO;
import ro.deloitte.digital.jhipster.conferences.service.mapper.ConferenceMapper;
import ro.deloitte.digital.jhipster.conferences.web.rest.errors.ExceptionTranslator;
import ro.deloitte.digital.jhipster.conferences.service.dto.ConferenceCriteria;
import ro.deloitte.digital.jhipster.conferences.service.ConferenceQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;


import static ro.deloitte.digital.jhipster.conferences.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ConferenceResource REST controller.
 *
 * @see ConferenceResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SecurityBeanOverrideConfiguration.class, ConferencesApp.class})
public class ConferenceResourceIntTest {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TECHNOLOGY = "AAAAAAAAAA";
    private static final String UPDATED_TECHNOLOGY = "BBBBBBBBBB";

    private static final Integer DEFAULT_CAPACITY = 1;
    private static final Integer UPDATED_CAPACITY = 2;

    @Autowired
    private ConferenceRepository conferenceRepository;

    @Autowired
    private ConferenceMapper conferenceMapper;

    @Autowired
    private ConferenceService conferenceService;

    @Autowired
    private ConferenceQueryService conferenceQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restConferenceMockMvc;

    private Conference conference;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ConferenceResource conferenceResource = new ConferenceResource(conferenceService, conferenceQueryService);
        this.restConferenceMockMvc = MockMvcBuilders.standaloneSetup(conferenceResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Conference createEntity(EntityManager em) {
        Conference conference = new Conference()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .technology(DEFAULT_TECHNOLOGY)
            .capacity(DEFAULT_CAPACITY);
        return conference;
    }

    @Before
    public void initTest() {
        conference = createEntity(em);
    }

    @Test
    @Transactional
    public void createConference() throws Exception {
        int databaseSizeBeforeCreate = conferenceRepository.findAll().size();

        // Create the Conference
        ConferenceDTO conferenceDTO = conferenceMapper.toDto(conference);
        restConferenceMockMvc.perform(post("/api/conferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conferenceDTO)))
            .andExpect(status().isCreated());

        // Validate the Conference in the database
        List<Conference> conferenceList = conferenceRepository.findAll();
        assertThat(conferenceList).hasSize(databaseSizeBeforeCreate + 1);
        Conference testConference = conferenceList.get(conferenceList.size() - 1);
        assertThat(testConference.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testConference.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testConference.getTechnology()).isEqualTo(DEFAULT_TECHNOLOGY);
        assertThat(testConference.getCapacity()).isEqualTo(DEFAULT_CAPACITY);
    }

    @Test
    @Transactional
    public void createConferenceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = conferenceRepository.findAll().size();

        // Create the Conference with an existing ID
        conference.setId(1L);
        ConferenceDTO conferenceDTO = conferenceMapper.toDto(conference);

        // An entity with an existing ID cannot be created, so this API call must fail
        restConferenceMockMvc.perform(post("/api/conferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conferenceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Conference in the database
        List<Conference> conferenceList = conferenceRepository.findAll();
        assertThat(conferenceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllConferences() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList
        restConferenceMockMvc.perform(get("/api/conferences?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(conference.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE.toString())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].technology").value(hasItem(DEFAULT_TECHNOLOGY.toString())))
            .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY)));
    }
    
    @Test
    @Transactional
    public void getConference() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get the conference
        restConferenceMockMvc.perform(get("/api/conferences/{id}", conference.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(conference.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE.toString()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.technology").value(DEFAULT_TECHNOLOGY.toString()))
            .andExpect(jsonPath("$.capacity").value(DEFAULT_CAPACITY));
    }

    @Test
    @Transactional
    public void getAllConferencesByTitleIsEqualToSomething() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where title equals to DEFAULT_TITLE
        defaultConferenceShouldBeFound("title.equals=" + DEFAULT_TITLE);

        // Get all the conferenceList where title equals to UPDATED_TITLE
        defaultConferenceShouldNotBeFound("title.equals=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllConferencesByTitleIsInShouldWork() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where title in DEFAULT_TITLE or UPDATED_TITLE
        defaultConferenceShouldBeFound("title.in=" + DEFAULT_TITLE + "," + UPDATED_TITLE);

        // Get all the conferenceList where title equals to UPDATED_TITLE
        defaultConferenceShouldNotBeFound("title.in=" + UPDATED_TITLE);
    }

    @Test
    @Transactional
    public void getAllConferencesByTitleIsNullOrNotNull() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where title is not null
        defaultConferenceShouldBeFound("title.specified=true");

        // Get all the conferenceList where title is null
        defaultConferenceShouldNotBeFound("title.specified=false");
    }

    @Test
    @Transactional
    public void getAllConferencesByDescriptionIsEqualToSomething() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where description equals to DEFAULT_DESCRIPTION
        defaultConferenceShouldBeFound("description.equals=" + DEFAULT_DESCRIPTION);

        // Get all the conferenceList where description equals to UPDATED_DESCRIPTION
        defaultConferenceShouldNotBeFound("description.equals=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllConferencesByDescriptionIsInShouldWork() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where description in DEFAULT_DESCRIPTION or UPDATED_DESCRIPTION
        defaultConferenceShouldBeFound("description.in=" + DEFAULT_DESCRIPTION + "," + UPDATED_DESCRIPTION);

        // Get all the conferenceList where description equals to UPDATED_DESCRIPTION
        defaultConferenceShouldNotBeFound("description.in=" + UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void getAllConferencesByDescriptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where description is not null
        defaultConferenceShouldBeFound("description.specified=true");

        // Get all the conferenceList where description is null
        defaultConferenceShouldNotBeFound("description.specified=false");
    }

    @Test
    @Transactional
    public void getAllConferencesByTechnologyIsEqualToSomething() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where technology equals to DEFAULT_TECHNOLOGY
        defaultConferenceShouldBeFound("technology.equals=" + DEFAULT_TECHNOLOGY);

        // Get all the conferenceList where technology equals to UPDATED_TECHNOLOGY
        defaultConferenceShouldNotBeFound("technology.equals=" + UPDATED_TECHNOLOGY);
    }

    @Test
    @Transactional
    public void getAllConferencesByTechnologyIsInShouldWork() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where technology in DEFAULT_TECHNOLOGY or UPDATED_TECHNOLOGY
        defaultConferenceShouldBeFound("technology.in=" + DEFAULT_TECHNOLOGY + "," + UPDATED_TECHNOLOGY);

        // Get all the conferenceList where technology equals to UPDATED_TECHNOLOGY
        defaultConferenceShouldNotBeFound("technology.in=" + UPDATED_TECHNOLOGY);
    }

    @Test
    @Transactional
    public void getAllConferencesByTechnologyIsNullOrNotNull() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where technology is not null
        defaultConferenceShouldBeFound("technology.specified=true");

        // Get all the conferenceList where technology is null
        defaultConferenceShouldNotBeFound("technology.specified=false");
    }

    @Test
    @Transactional
    public void getAllConferencesByCapacityIsEqualToSomething() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where capacity equals to DEFAULT_CAPACITY
        defaultConferenceShouldBeFound("capacity.equals=" + DEFAULT_CAPACITY);

        // Get all the conferenceList where capacity equals to UPDATED_CAPACITY
        defaultConferenceShouldNotBeFound("capacity.equals=" + UPDATED_CAPACITY);
    }

    @Test
    @Transactional
    public void getAllConferencesByCapacityIsInShouldWork() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where capacity in DEFAULT_CAPACITY or UPDATED_CAPACITY
        defaultConferenceShouldBeFound("capacity.in=" + DEFAULT_CAPACITY + "," + UPDATED_CAPACITY);

        // Get all the conferenceList where capacity equals to UPDATED_CAPACITY
        defaultConferenceShouldNotBeFound("capacity.in=" + UPDATED_CAPACITY);
    }

    @Test
    @Transactional
    public void getAllConferencesByCapacityIsNullOrNotNull() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where capacity is not null
        defaultConferenceShouldBeFound("capacity.specified=true");

        // Get all the conferenceList where capacity is null
        defaultConferenceShouldNotBeFound("capacity.specified=false");
    }

    @Test
    @Transactional
    public void getAllConferencesByCapacityIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where capacity greater than or equals to DEFAULT_CAPACITY
        defaultConferenceShouldBeFound("capacity.greaterOrEqualThan=" + DEFAULT_CAPACITY);

        // Get all the conferenceList where capacity greater than or equals to UPDATED_CAPACITY
        defaultConferenceShouldNotBeFound("capacity.greaterOrEqualThan=" + UPDATED_CAPACITY);
    }

    @Test
    @Transactional
    public void getAllConferencesByCapacityIsLessThanSomething() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        // Get all the conferenceList where capacity less than or equals to DEFAULT_CAPACITY
        defaultConferenceShouldNotBeFound("capacity.lessThan=" + DEFAULT_CAPACITY);

        // Get all the conferenceList where capacity less than or equals to UPDATED_CAPACITY
        defaultConferenceShouldBeFound("capacity.lessThan=" + UPDATED_CAPACITY);
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultConferenceShouldBeFound(String filter) throws Exception {
        restConferenceMockMvc.perform(get("/api/conferences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(conference.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].technology").value(hasItem(DEFAULT_TECHNOLOGY)))
            .andExpect(jsonPath("$.[*].capacity").value(hasItem(DEFAULT_CAPACITY)));

        // Check, that the count call also returns 1
        restConferenceMockMvc.perform(get("/api/conferences/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultConferenceShouldNotBeFound(String filter) throws Exception {
        restConferenceMockMvc.perform(get("/api/conferences?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restConferenceMockMvc.perform(get("/api/conferences/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingConference() throws Exception {
        // Get the conference
        restConferenceMockMvc.perform(get("/api/conferences/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateConference() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        int databaseSizeBeforeUpdate = conferenceRepository.findAll().size();

        // Update the conference
        Conference updatedConference = conferenceRepository.findById(conference.getId()).get();
        // Disconnect from session so that the updates on updatedConference are not directly saved in db
        em.detach(updatedConference);
        updatedConference
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .technology(UPDATED_TECHNOLOGY)
            .capacity(UPDATED_CAPACITY);
        ConferenceDTO conferenceDTO = conferenceMapper.toDto(updatedConference);

        restConferenceMockMvc.perform(put("/api/conferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conferenceDTO)))
            .andExpect(status().isOk());

        // Validate the Conference in the database
        List<Conference> conferenceList = conferenceRepository.findAll();
        assertThat(conferenceList).hasSize(databaseSizeBeforeUpdate);
        Conference testConference = conferenceList.get(conferenceList.size() - 1);
        assertThat(testConference.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testConference.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testConference.getTechnology()).isEqualTo(UPDATED_TECHNOLOGY);
        assertThat(testConference.getCapacity()).isEqualTo(UPDATED_CAPACITY);
    }

    @Test
    @Transactional
    public void updateNonExistingConference() throws Exception {
        int databaseSizeBeforeUpdate = conferenceRepository.findAll().size();

        // Create the Conference
        ConferenceDTO conferenceDTO = conferenceMapper.toDto(conference);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restConferenceMockMvc.perform(put("/api/conferences")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(conferenceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Conference in the database
        List<Conference> conferenceList = conferenceRepository.findAll();
        assertThat(conferenceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteConference() throws Exception {
        // Initialize the database
        conferenceRepository.saveAndFlush(conference);

        int databaseSizeBeforeDelete = conferenceRepository.findAll().size();

        // Delete the conference
        restConferenceMockMvc.perform(delete("/api/conferences/{id}", conference.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Conference> conferenceList = conferenceRepository.findAll();
        assertThat(conferenceList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Conference.class);
        Conference conference1 = new Conference();
        conference1.setId(1L);
        Conference conference2 = new Conference();
        conference2.setId(conference1.getId());
        assertThat(conference1).isEqualTo(conference2);
        conference2.setId(2L);
        assertThat(conference1).isNotEqualTo(conference2);
        conference1.setId(null);
        assertThat(conference1).isNotEqualTo(conference2);
    }

    @Test
    @Transactional
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ConferenceDTO.class);
        ConferenceDTO conferenceDTO1 = new ConferenceDTO();
        conferenceDTO1.setId(1L);
        ConferenceDTO conferenceDTO2 = new ConferenceDTO();
        assertThat(conferenceDTO1).isNotEqualTo(conferenceDTO2);
        conferenceDTO2.setId(conferenceDTO1.getId());
        assertThat(conferenceDTO1).isEqualTo(conferenceDTO2);
        conferenceDTO2.setId(2L);
        assertThat(conferenceDTO1).isNotEqualTo(conferenceDTO2);
        conferenceDTO1.setId(null);
        assertThat(conferenceDTO1).isNotEqualTo(conferenceDTO2);
    }

    @Test
    @Transactional
    public void testEntityFromId() {
        assertThat(conferenceMapper.fromId(42L).getId()).isEqualTo(42);
        assertThat(conferenceMapper.fromId(null)).isNull();
    }
}
