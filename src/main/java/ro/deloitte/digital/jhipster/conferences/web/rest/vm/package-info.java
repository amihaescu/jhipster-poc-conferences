/**
 * View Models used by Spring MVC REST controllers.
 */
package ro.deloitte.digital.jhipster.conferences.web.rest.vm;
