package ro.deloitte.digital.jhipster.conferences.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import ro.deloitte.digital.jhipster.conferences.domain.Conference;
import ro.deloitte.digital.jhipster.conferences.domain.*; // for static metamodels
import ro.deloitte.digital.jhipster.conferences.repository.ConferenceRepository;
import ro.deloitte.digital.jhipster.conferences.service.dto.ConferenceCriteria;
import ro.deloitte.digital.jhipster.conferences.service.dto.ConferenceDTO;
import ro.deloitte.digital.jhipster.conferences.service.mapper.ConferenceMapper;

/**
 * Service for executing complex queries for Conference entities in the database.
 * The main input is a {@link ConferenceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link ConferenceDTO} or a {@link Page} of {@link ConferenceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ConferenceQueryService extends QueryService<Conference> {

    private final Logger log = LoggerFactory.getLogger(ConferenceQueryService.class);

    private final ConferenceRepository conferenceRepository;

    private final ConferenceMapper conferenceMapper;

    public ConferenceQueryService(ConferenceRepository conferenceRepository, ConferenceMapper conferenceMapper) {
        this.conferenceRepository = conferenceRepository;
        this.conferenceMapper = conferenceMapper;
    }

    /**
     * Return a {@link List} of {@link ConferenceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<ConferenceDTO> findByCriteria(ConferenceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Conference> specification = createSpecification(criteria);
        return conferenceMapper.toDto(conferenceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link ConferenceDTO} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<ConferenceDTO> findByCriteria(ConferenceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Conference> specification = createSpecification(criteria);
        return conferenceRepository.findAll(specification, page)
            .map(conferenceMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(ConferenceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<Conference> specification = createSpecification(criteria);
        return conferenceRepository.count(specification);
    }

    /**
     * Function to convert ConferenceCriteria to a {@link Specification}
     */
    private Specification<Conference> createSpecification(ConferenceCriteria criteria) {
        Specification<Conference> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Conference_.id));
            }
            if (criteria.getTitle() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTitle(), Conference_.title));
            }
            if (criteria.getDescription() != null) {
                specification = specification.and(buildStringSpecification(criteria.getDescription(), Conference_.description));
            }
            if (criteria.getTechnology() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTechnology(), Conference_.technology));
            }
            if (criteria.getCapacity() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCapacity(), Conference_.capacity));
            }
        }
        return specification;
    }
}
