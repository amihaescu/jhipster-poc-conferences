package ro.deloitte.digital.jhipster.conferences.service.mapper;

import ro.deloitte.digital.jhipster.conferences.domain.*;
import ro.deloitte.digital.jhipster.conferences.service.dto.ConferenceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity Conference and its DTO ConferenceDTO.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ConferenceMapper extends EntityMapper<ConferenceDTO, Conference> {



    default Conference fromId(Long id) {
        if (id == null) {
            return null;
        }
        Conference conference = new Conference();
        conference.setId(id);
        return conference;
    }
}
