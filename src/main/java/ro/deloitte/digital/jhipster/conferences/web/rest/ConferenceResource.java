package ro.deloitte.digital.jhipster.conferences.web.rest;
import ro.deloitte.digital.jhipster.conferences.service.ConferenceService;
import ro.deloitte.digital.jhipster.conferences.web.rest.errors.BadRequestAlertException;
import ro.deloitte.digital.jhipster.conferences.web.rest.util.HeaderUtil;
import ro.deloitte.digital.jhipster.conferences.service.dto.ConferenceDTO;
import ro.deloitte.digital.jhipster.conferences.service.dto.ConferenceCriteria;
import ro.deloitte.digital.jhipster.conferences.service.ConferenceQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Conference.
 */
@RestController
@RequestMapping("/api")
public class ConferenceResource {

    private final Logger log = LoggerFactory.getLogger(ConferenceResource.class);

    private static final String ENTITY_NAME = "conferencesConference";

    private final ConferenceService conferenceService;

    private final ConferenceQueryService conferenceQueryService;

    public ConferenceResource(ConferenceService conferenceService, ConferenceQueryService conferenceQueryService) {
        this.conferenceService = conferenceService;
        this.conferenceQueryService = conferenceQueryService;
    }

    /**
     * POST  /conferences : Create a new conference.
     *
     * @param conferenceDTO the conferenceDTO to create
     * @return the ResponseEntity with status 201 (Created) and with body the new conferenceDTO, or with status 400 (Bad Request) if the conference has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/conferences")
    public ResponseEntity<ConferenceDTO> createConference(@RequestBody ConferenceDTO conferenceDTO) throws URISyntaxException {
        log.debug("REST request to save Conference : {}", conferenceDTO);
        if (conferenceDTO.getId() != null) {
            throw new BadRequestAlertException("A new conference cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ConferenceDTO result = conferenceService.save(conferenceDTO);
        return ResponseEntity.created(new URI("/api/conferences/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /conferences : Updates an existing conference.
     *
     * @param conferenceDTO the conferenceDTO to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated conferenceDTO,
     * or with status 400 (Bad Request) if the conferenceDTO is not valid,
     * or with status 500 (Internal Server Error) if the conferenceDTO couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/conferences")
    public ResponseEntity<ConferenceDTO> updateConference(@RequestBody ConferenceDTO conferenceDTO) throws URISyntaxException {
        log.debug("REST request to update Conference : {}", conferenceDTO);
        if (conferenceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ConferenceDTO result = conferenceService.save(conferenceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, conferenceDTO.getId().toString()))
            .body(result);
    }

    /**
     * GET  /conferences : get all the conferences.
     *
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of conferences in body
     */
    @GetMapping("/conferences")
    public ResponseEntity<List<ConferenceDTO>> getAllConferences(ConferenceCriteria criteria) {
        log.debug("REST request to get Conferences by criteria: {}", criteria);
        List<ConferenceDTO> entityList = conferenceQueryService.findByCriteria(criteria);
        return ResponseEntity.ok().body(entityList);
    }

    /**
    * GET  /conferences/count : count all the conferences.
    *
    * @param criteria the criterias which the requested entities should match
    * @return the ResponseEntity with status 200 (OK) and the count in body
    */
    @GetMapping("/conferences/count")
    public ResponseEntity<Long> countConferences(ConferenceCriteria criteria) {
        log.debug("REST request to count Conferences by criteria: {}", criteria);
        return ResponseEntity.ok().body(conferenceQueryService.countByCriteria(criteria));
    }

    /**
     * GET  /conferences/:id : get the "id" conference.
     *
     * @param id the id of the conferenceDTO to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the conferenceDTO, or with status 404 (Not Found)
     */
    @GetMapping("/conferences/{id}")
    public ResponseEntity<ConferenceDTO> getConference(@PathVariable Long id) {
        log.debug("REST request to get Conference : {}", id);
        Optional<ConferenceDTO> conferenceDTO = conferenceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(conferenceDTO);
    }

    /**
     * DELETE  /conferences/:id : delete the "id" conference.
     *
     * @param id the id of the conferenceDTO to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/conferences/{id}")
    public ResponseEntity<Void> deleteConference(@PathVariable Long id) {
        log.debug("REST request to delete Conference : {}", id);
        conferenceService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
