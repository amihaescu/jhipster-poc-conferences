package ro.deloitte.digital.jhipster.conferences.service.dto;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the Conference entity.
 */
public class ConferenceDTO implements Serializable {

    private Long id;

    private String title;

    private String description;

    private String technology;

    private Integer capacity;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ConferenceDTO conferenceDTO = (ConferenceDTO) o;
        if (conferenceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), conferenceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "ConferenceDTO{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", technology='" + getTechnology() + "'" +
            ", capacity=" + getCapacity() +
            "}";
    }
}
